import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyClB9xZvmh1Ra-RL5W6HxO_Y7sTwsChfk0",
    authDomain: "ninja-smoothies-ada6a.firebaseapp.com",
    databaseURL: "https://ninja-smoothies-ada6a.firebaseio.com",
    projectId: "ninja-smoothies-ada6a",
    storageBucket: "ninja-smoothies-ada6a.appspot.com",
    messagingSenderId: "613628900265",
    appId: "1:613628900265:web:cfdf45f6be8f4d80e74ab8",
    measurementId: "G-RZLC65XRR0"
  };
  
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore().settings({timestampsInSnapshots: true})

firebase.analytics();

//export firestore database
export default firebaseApp.firestore()


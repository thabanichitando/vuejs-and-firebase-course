import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyBw5n1QGWAHJXT4vDL5AGmvSIm2qY7E0oo",
    authDomain: "ninja-geo-3489a.firebaseapp.com",
    databaseURL: "https://ninja-geo-3489a.firebaseio.com",
    projectId: "ninja-geo-3489a",
    storageBucket: "ninja-geo-3489a.appspot.com",
    messagingSenderId: "943997835987",
    appId: "1:943997835987:web:5c1c5168b6ad52506b1226",
    measurementId: "G-W3967Q2CEQ"
  };
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore()
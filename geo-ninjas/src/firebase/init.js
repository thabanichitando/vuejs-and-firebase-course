import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
  apiKey: "AIzaSyDUSVQww6IkMLqW5T-5MA1xDd7AZqyx3G8",
  authDomain: "geo-ninjas-bf181.firebaseapp.com",
  databaseURL: "https://geo-ninjas-bf181.firebaseio.com",
  projectId: "geo-ninjas-bf181",
  storageBucket: "geo-ninjas-bf181.appspot.com",
  messagingSenderId: "142391964407",
  appId: "1:142391964407:web:99ed0f3b42b88174e819f2",
  measurementId: "G-MCW1ZMYZZL"
};

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebaseApp.firestore().settings({ timestampsInSnapshots: true })
  
  export default firebaseApp.firestore()
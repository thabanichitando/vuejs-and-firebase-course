import firebase from 'firebase'
import firestore from 'firebase/firestore'


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBKMDqd3Pyy1vamFCN9eM5XFdtLb23QXLA",
  authDomain: "ninja-chat-3c94f.firebaseapp.com",
  databaseURL: "https://ninja-chat-3c94f.firebaseio.com",
  projectId: "ninja-chat-3c94f",
  storageBucket: "ninja-chat-3c94f.appspot.com",
  messagingSenderId: "787583851493",
  appId: "1:787583851493:web:637fb4c68bc3e48c95ed0c",
  measurementId: "G-7885JNNPBJ"
};
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  // firebaseApp.firestore().settings({timestampsInSnapshots: true})

  export default firebaseApp.firestore()